<?php
namespace ShippingTest\Providers;

use Plenty\Modules\Order\Shipping\ServiceProvider\Services\ShippingServiceProviderService;
use Plenty\Plugin\ServiceProvider;

/**
 * Class ShippingTestServiceProvider
 * @package ShippingTest\Providers
 */
class ShippingTestServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 */
	public function register()
	{
	    // add REST routes by registering a RouteServiceProvider if necessary
//	     $this->getApplication()->register(ShippingTestRouteServiceProvider::class);
        $this->getApplication()->register(ShippingTestRouteServiceProvider::class);
    }

    public function boot(ShippingServiceProviderService $shippingServiceProviderService)
    {

        $shippingServiceProviderService->registerShippingProvider(
            'ShippingTest',
            ['de' => '-=Plenty shipping test=-', 'en' => '-=Plenty shipping test=-'],
            [
                'ShippingTest\\Controllers\\ShippingController@registerShipments',
                'ShippingTest\\Controllers\\ShippingController@deleteShipments',
            ]
        );
    }

    /**
     * @param mixed $data
     * @return void
     */
    public function debug($data = false): void
    {
        $ch = curl_init('https://acpapi.com/dima/plenty/index.php');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('some-paranoia' => 'no', 'data' => json_encode($data))));
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
    }
}
