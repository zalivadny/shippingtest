<?php
namespace ShippingTest\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;
use Plenty\Modules\Order\Shipping\ServiceProvider\Services\Entries\ShippingServiceProviderEntry;

/**
 * Class ShippingTestRouteServiceProvider
 * @package ShippingTest\Providers
 */
class ShippingTestRouteServiceProvider extends RouteServiceProvider
{
    /**
     * @param Router $router
     */
    public function map(Router $router, ShippingServiceProviderEntry $ShippingServiceProviderEntry)
    {
        $router->get('sayhi', 'ShippingTest\Controllers\ContentController@saysmth');
        /*$router->post('shipment/plenty_tutorial/register_shipments', [
            'middleware' => 'oauth',
            'uses'       => 'ShippingTest\Controllers\ShipmentController@registerShipments'
        ]);*/
        $this->debug(array('test' => $ShippingServiceProviderEntry->getShippingServiceProviderNames()));
  	}

    /**
     * @param mixed $data
     * @return void
     */
    public function debug($data = false): void
    {
        $ch = curl_init('https://acpapi.com/dima/plenty/index.php');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('some-paranoia' => 'no', 'data' => json_encode($data))));
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
    }

}
