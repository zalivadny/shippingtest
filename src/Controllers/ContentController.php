<?php

namespace ShippingTest\Controllers;


use Plenty\Plugin\Controller;
use Plenty\Plugin\Templates\Twig;
use Plenty\Plugin\ConfigRepository;
use Plenty\Modules\Plugin\Storage\Contracts\StorageRepositoryContract;

class ContentController extends Controller
{

    /**
     * @var StorageRepositoryContract $storageRepository
     */
    private $storageRepository;

    public function saysmth(Twig $twig, ConfigRepository $config, StorageRepositoryContract $storageRepository): string
    {

        $this->storageRepository = $storageRepository;

        $conf = array();
        $conf['mode'] = $config->get('ShippingTest.mode', '0');
        $conf['apiKey'] = $config->get('ShippingTest.apiKey', '');
        $conf['ShipperCompanyName'] = $config->get('ShippingTest.ShipperCompanyName', '');
        $conf['HScode'] = $config->get('ShippingTest.HScode', '');
        $conf['DeclarationType'] = $config->get('ShippingTest.DeclarationType', '');
        $conf['LabelSettings'] = $config->get('ShippingTest.LabelSettings', '');

        $key = 'labels/label1.txt';

        $_inco = '1234567890<br>\n';
        $inco = '1234567890<br>';
        $i=10000;
        while($i--) {
            $inco .= $_inco;
        }

        $a = [];
        $a = $this->storageRepository->uploadObject('ShippingTest', $key, $inco, true);

//        $storedData = $this->storageRepository->getObject('ShippingTest', $key);
        $storedData = [];
        $storedData[] = $this->storageRepository->listObjects('ShippingTest','1', 100);
        $storedData[] = $this->storageRepository->listObjects('ShippingTest','t', 100);
        $storedData[] = $this->storageRepository->listObjects('ShippingTest','z', 100);

        $storedData[] = '---------------------------------------------------------------------';
        $storedData[] = $this->storageRepository->getObjectUrl('ShippingTest', $key, true, 14);


        $templateData = array(
            'mode' => $conf['mode'],
            'resultCount' => $conf['apiKey'],
            'ShipperCompanyName' => $conf['ShipperCompanyName'],
            'HScode' => $conf['HScode'],
            'DeclarationType' => $conf['DeclarationType'],
            'LabelSettings' => $conf['LabelSettings'],
            'storedData' => json_encode(array($a, $storedData))
        );

        return $twig->render('ShippingTest::content.sayhi', $templateData);
    }
}